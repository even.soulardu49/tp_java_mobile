package com.example.tp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * The class who permit to get and set the settings in shared preferences.
 */
public class SettingSharedPreferences {

  private final Context context;

  public SettingSharedPreferences(Context context) {
      this.context = context;
  }


  public Setting getSettings() {
    Setting setting = new Setting();

    SharedPreferences preferences =  context.getSharedPreferences("settings", 0);

    int bgColor = preferences.getInt("bgColor", 0xffffffff);
    String currency = preferences.getString("currency", "€");
    String salaryType = preferences.getString("salaryType", "month");

    setting.setBgColor(bgColor);
    setting.setCurrency(currency);
    setting.setSalaryType(salaryType);

    return setting;
  }

  public void setSettings(Setting settings) {

    SharedPreferences preferences = context.getSharedPreferences("settings", 0);

    SharedPreferences.Editor prefsEditor = preferences.edit();

    prefsEditor.putInt("bgColor", settings.getBgColor());
    prefsEditor.putString("currency", settings.getCurrency());
    prefsEditor.putString("salaryType", settings.getSalaryType());

    prefsEditor.apply();
  }

}

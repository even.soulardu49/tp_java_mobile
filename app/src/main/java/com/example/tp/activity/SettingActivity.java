package com.example.tp.activity;

import android.content.Intent;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import com.example.tp.R;
import com.example.tp.Setting;
import com.example.tp.SettingSharedPreferences;
import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * The setting page.
 */
public class SettingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

  private Setting settings;
  private SettingSharedPreferences settingSharedPreferences;
  private RelativeLayout settingLayout;
  Button changeColorBtn;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_setting);

    //Stock shared preferences in Setting object
    settingSharedPreferences = new SettingSharedPreferences(this);
    settings = settingSharedPreferences.getSettings();

    //Set background color by the color stock in shared preferences
    settingLayout = findViewById(R.id.settingLayout);
    settingLayout.setBackgroundColor(settings.getBgColor());

    //Create currency spinner
    Spinner currencySpinner = findViewById(R.id.currencySpinner);
    ArrayAdapter<CharSequence> currencyAdapter = ArrayAdapter.createFromResource(this,
        R.array.currency_array, android.R.layout.simple_spinner_item);
    currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    currencySpinner.setAdapter(currencyAdapter);
    currencySpinner.setSelection(currencyAdapter.getPosition(settings.getCurrency()));
    currencySpinner.setOnItemSelectedListener(this);


    //create salary type spinner
    Spinner salaryTypeSpinner = findViewById(R.id.salaryTypeSpinner);
    ArrayAdapter<CharSequence> salaryAdapter = ArrayAdapter.createFromResource(this,
        R.array.salary_type_array, android.R.layout.simple_spinner_item);
    salaryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    salaryTypeSpinner.setAdapter(salaryAdapter);
    salaryTypeSpinner.setSelection(salaryAdapter.getPosition(settings.getSalaryType()));
    salaryTypeSpinner.setOnItemSelectedListener(this);

    //create change background color button
    changeColorBtn = findViewById(R.id.changeColorButton);
    changeColorBtn.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            openColorPicker(); 
          }
        }
    );

    //Stock in sharedPreferences the new settings (in Setting object) enter by the user on validate button click and go to main  page.
    Button validateSettingBtn = findViewById(R.id.validateSettingBtn);
    validateSettingBtn.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            settingSharedPreferences.setSettings(settings);

            Intent myIntent = new Intent(SettingActivity.this, MainActivity.class);
            startActivity(myIntent);
          }
        }
    );

  }

  //Method when an item of a spinner is selected (set Setting object)
  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    Spinner currencySpinner = (Spinner) parent;
    Spinner salaryTypeSpinner = (Spinner) parent;

    if(currencySpinner.getId() == R.id.currencySpinner)
    {
      String currency = (String) currencySpinner.getItemAtPosition(position);
      settings.setCurrency(currency);
    }
    if(salaryTypeSpinner.getId() == R.id.salaryTypeSpinner)
    {
      String salaryType = (String) salaryTypeSpinner.getItemAtPosition(position);
      settings.setSalaryType(salaryType);
    }
  }

  //Necessary because of the implementation class AdapterView.OnItemSelectedListener.
  @Override
  public void onNothingSelected(AdapterView<?> parent) {

  }

  //Permit to open the color picker
  private void openColorPicker() {
    AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, settings.getBgColor(), new AmbilWarnaDialog.OnAmbilWarnaListener() {
      @Override
      public void onCancel(AmbilWarnaDialog dialog) {

      }

      //on color picker ok set Setting object.
      @Override
      public void onOk(AmbilWarnaDialog dialog, int color) {
          settings.setBgColor(color);
      }
    });
    colorPicker.show();
  }
}
package com.example.tp.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.tp.R;
import com.example.tp.Setting;
import com.example.tp.SettingSharedPreferences;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

/**
 * The main page.
 */
public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    //Stock shared preferences in Setting object
    SettingSharedPreferences settingSharedPreferences = new SettingSharedPreferences(this);
    final Setting settings = settingSharedPreferences.getSettings();

    //Set background color by the color stock in shared preferences
    RelativeLayout mainLayout = findViewById(R.id.mainLayout);
    mainLayout.setBackgroundColor(settings.getBgColor());

    //Set field text by the correct currency and salary type
    final TextView textViewField = findViewById(R.id.fieldTextTitle);
    String salaryTypeTxt = settings.getSalaryType().equals("year") ? "annuel" : "mensuel";
    textViewField.setText("Entrez votre salaire " + salaryTypeTxt + " en " + settings.getCurrency());


    //When the button submit is click, calculate and show the result on the textview.
    final EditText editTextSalary = findViewById(R.id.editTextSalary);
    final Button submitBtn = findViewById(R.id.submitSalaryBtn);
    final TextView textViewResult = findViewById(R.id.textViewResult);
    submitBtn.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {

        if(!editTextSalary.getText().toString().isEmpty()){
          double brutSalary = Double.parseDouble(editTextSalary.getText().toString());

          double hourNetSalary;
          double monthNetSalary;
          double yearNetSalary;

          if(settings.getSalaryType().equals("year")){
            yearNetSalary = brutSalary - brutSalary * 0.2;
            monthNetSalary = yearNetSalary / 12;
          }else{
            monthNetSalary = brutSalary - brutSalary * 20/100;
            yearNetSalary = monthNetSalary * 12;
          }
          hourNetSalary = monthNetSalary / 140;

          textViewResult.setText(
              "Salaire net annuel : " + String.format("%.2f", yearNetSalary) + settings.getCurrency() +
                  "\nSalaire net mensuel : " + String.format("%.2f", monthNetSalary) + settings.getCurrency() +
                  "\nSalaire net horaire : " + String.format("%.2f", hourNetSalary) + settings.getCurrency()
          );
        }


      }
    });


    //Just the floating action button who permits to go to the setting page.
    final FloatingActionButton settingBtn = findViewById(R.id.floatingSettingsBtn);

    settingBtn.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        Intent myIntent = new Intent(MainActivity.this, SettingActivity.class);
        startActivity(myIntent);
      }
    });
  }

}
package com.example.tp;

/**
 * Setting model.
 *
 * Contains background color, currency and salary type.
 */
public class Setting {

  private int bgColor;
  private String currency;
  private String salaryType;

  public int getBgColor() {
    return bgColor;
  }

  public void setBgColor(int bgColor) {
    this.bgColor = bgColor;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getSalaryType() {
    return salaryType;
  }

  public void setSalaryType(String salaryType) {
    this.salaryType = salaryType;
  }

  @Override
  public String toString() {
    return "Setting{" +
        ", bgColor=" + bgColor +
        ", currency='" + currency + '\'' +
        ", salaryType='" + salaryType + '\'' +
        '}';
  }
}
